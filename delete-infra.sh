#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source $SCRIPT_DIR/envvars
IFS=$'\n'
yellow='\033[1;33m'
nc='\033[0m' # No Color
#Security pieces
gcloud beta network-services endpoint-policies delete -q ${CLUSTER}-endpoint-policy --location=global

echo -e "Deleting ${yellow}Forwarding Rules${nc}."
for fwdrule in $(gcloud compute forwarding-rules list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute forwarding-rules delete -q --global $fwdrule &
done
wait
echo -e "Deleting ${yellow}External Target HTTPS Proxy${nc}."
for targets in $(gcloud compute target-https-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute target-https-proxies delete -q $targets --global &
done
wait
echo -e "Deleting ${yellow}Target gRPC Proxies${nc}."
for targets in $(gcloud compute target-grpc-proxies list --format="value(name)" --filter="name~'${CLUSTER}-grpc-proxy'")
do
  gcloud compute target-grpc-proxies delete -q $targets &
done
wait
echo -e "Deleting ${yellow}URL Maps${nc}."
for url_maps in $(gcloud compute url-maps list --format="value(name)" --filter="name~'${CLUSTER}-ext-map|${CLUSTER}-url-map'")
do
  gcloud compute url-maps delete -q --global  $url_maps &
done
wait
echo -e "Deleting ${yellow}Backend Services${nc}."
for bes in $(gcloud compute backend-services list --format="value(name)" --filter="name~'^${CLUSTER}.*-vm-bes$'")
do
  gcloud compute backend-services delete -q --global $bes
done
echo -e "Deleting ${yellow}Compute Engine Managed Instance Groups${nc}."
for mig in $(gcloud compute instance-groups managed list --format="value(name,zone)" --filter="name~'^${CLUSTER}-mig\-'")
do
  gcloud compute instance-groups managed delete -q $(echo $mig | cut -f 1) --zone=$(echo $mig | cut -f 2)
done
wait
echo -e "Deleting ${yellow}Instance Templates${nc}."
for template in $(gcloud compute instance-templates list --format="value(name)" --filter="name~'\-tpl\-'")
do
  gcloud compute instance-templates delete -q $template
done
wait
echo -e "Deleting ${yellow}External Target HTTPS Proxy${nc}."
for targets in $(gcloud compute target-https-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute target-https-proxies delete -q $targets --global &
done
wait
echo -e "Deleting ${yellow}External Target HTTP Proxy${nc}."
for targets in $(gcloud compute target-http-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute target-http-proxies delete -q $targets --global &
done
wait
echo -e "Deleting ${yellow}Target TCP Proxies${nc}."
for targets in $(gcloud compute target-tcp-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute target-tcp-proxies delete -q $targets &
done
wait
echo -e "Deleting ${yellow}Target gRPC Proxies${nc}."
for targets in $(gcloud compute target-grpc-proxies list --format="value(name)" --filter="name~'${CLUSTER}-grpc-proxy'")
do
  gcloud compute target-grpc-proxies delete -q $targets &
done
wait
echo -e "Deleting ${yellow}URL Maps${nc}."
for url_maps in $(gcloud compute url-maps list --format="value(name)" --filter="name~'${CLUSTER}-ext-map|${CLUSTER}-url-map'")
do
  gcloud compute url-maps delete -q --global  $url_maps &
done
wait
echo -e "Deleting ${yellow}Backend Services${nc}."
for backend_services in $(gcloud compute backend-services list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute backend-services delete --global -q $backend_services &
done
wait
echo -e "Deleting ${yellow}Network Endpoint Groups${nc}."
for neg in $(gcloud compute network-endpoint-groups list --format="value(name,zone)" --filter="name~'-neg.*'")
do
  gcloud compute network-endpoint-groups delete -q $(echo $neg | cut -f 1) --zone=$(echo $neg | cut -f 2) &
done
wait
echo -e "Deleting ${yellow}SSL Certificate${nc}."
gcloud compute ssl-certificates delete ${CLUSTER}-ext-cert --global -q

echo -e "Deleting ${yellow}Reserved IPv4 Address${nc}."
gcloud compute addresses delete ${CLUSTER}-ext-vip --global -q

echo -e "Deleting ${yellow}Forwarding Rules${nc}."
for fwdrule in $(gcloud compute forwarding-rules list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute forwarding-rules delete -q --global $fwdrule &
done
wait
echo -e "Deleting ${yellow}Target TCP Proxies${nc}."
for targets in $(gcloud compute target-tcp-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute target-tcp-proxies delete -q $targets &
done
wait
echo -e "Deleting ${yellow}Target gRPC Proxies${nc}."
for targets in $(gcloud compute target-grpc-proxies list --format="value(name)" --filter="name~'${CLUSTER}-grpc-proxy'")
do
  gcloud compute target-grpc-proxies delete -q $targets &
done
wait
echo -e "Deleting ${yellow}URL Maps${nc}."
for url_maps in $(gcloud compute url-maps list --format="value(name)" --filter="name~'${CLUSTER}-ext-map|${CLUSTER}-url-map'")
do
  gcloud compute url-maps delete -q --global  $url_maps &
done
wait
echo -e "Deleting ${yellow}Backend Services${nc}."
for backend_services in $(gcloud compute backend-services list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute backend-services delete --global -q $backend_services &
done
wait
echo -e "Deleting ${yellow}Health Checks${nc}."
for health_checks in $(gcloud compute health-checks list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute health-checks delete --global -q $health_checks &
done
wait
echo -e "Deleting ${yellow}GKE Clusters in zones ${CLUSTER1ZONE}${nc} and ${yellow}${CLUSTER2ZONE}${nc}."
gcloud container clusters delete "${CLUSTER}-${CLUSTER1ZONE}" --zone "${CLUSTER1ZONE}" -q &
gcloud container clusters delete "${CLUSTER}-${CLUSTER2ZONE}" --zone "${CLUSTER2ZONE}" -q
echo -e "Deleting ${yellow}GKE Hub Entries${nc}."
gcloud container hub memberships delete ${CLUSTER}-${CLUSTER1ZONE} -q &
gcloud container hub memberships delete ${CLUSTER}-${CLUSTER2ZONE} -q
unset IFS
wait
echo -e "Deleting Pub/Sub Topics and Subscriptions${nc}."
TOPICS="adservice cartservice checkoutservice currencyservice emailservice frontend managementloggingservice managementservice paymentservice productcatalogservice recommendationservice shippingservice shippingservice-enhanced"
for topic in $TOPICS
do
  gcloud pubsub topics delete $topic -q &
done
wait
for sub in $(gcloud pubsub subscriptions list --format="value(name)" --filter="name~'$(echo $TOPICS | sed s/' '/'|'/g)'")
do
  gcloud pubsub subscriptions delete $sub -q &
done
wait
echo -e "Deleting ${yellow}Firewall Rules${nc}."
gcloud compute firewall-rules delete ${CLUSTER}-hc-${NETWORKNAME} -q &
gcloud compute firewall-rules delete ${CLUSTER}-int-${NETWORKNAME} -q
wait
echo -e "Removing ${yellow}Project-Specific Data${nc}."
rm -rf $SCRIPT_DIR/td-sidecar-injector-xdsv3
rm $SCRIPT_DIR/dns-spec.yaml