#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-hipsterdemo}"
PROJECT_ID=${PROJECT_ID:-$(gcloud config get-value project)}
IDENTITY_NAMESPACE="${PROJECT_ID}.svc.id.goog"
K8S_NAMESPACE="default"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Switch back to GKE only topology by updating ${yellow}URL Maps${nc}"
gcloud compute url-maps remove-path-matcher ${CLUSTER}-url-map \
  --path-matcher-name pathmatch-checkoutservice
gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
  --default-service ${CLUSTER}-checkoutservice-bes \
  --path-matcher-name pathmatch-checkoutservice --new-hosts="checkoutservice:8080"

echo -e "Creating ${yellow}Client TLS policy${nc}."
cat << EOF > client-mtls-policy.yaml
name: "${CLUSTER}-client-mtls-policy"
clientCertificate:
  certificateProviderInstance:
    pluginInstance: google_cloud_private_spiffe
serverValidationCa:
- certificateProviderInstance:
    pluginInstance: google_cloud_private_spiffe
EOF
gcloud alpha network-security client-tls-policies import ${CLUSTER}-client-mtls-policy \
  --source=./client-mtls-policy.yaml --location=global
rm ./client-mtls-policy.yaml

echo -e "Attaching ${yellow}client policy to backend services${nc}."
SERVICES="cartservice productcatalogservice currencyservice paymentservice shippingservice shippingservice-enhanced emailservice recommendationservice adservice checkoutservice"
for service in $SERVICES
do
  echo "Updating ${service} with client policy."
  gcloud beta compute backend-services export ${CLUSTER}-${service}-bes --global \
    --destination=backend-service.yaml

  cat << EOF >> backend-service.yaml
securitySettings:
  clientTlsPolicy: projects/${PROJECT_ID}/locations/global/clientTlsPolicies/${CLUSTER}-client-mtls-policy
  subjectAltNames:
    - "spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/${service}"
EOF
  gcloud beta compute backend-services import ${CLUSTER}-${service}-bes --global \
    --source=backend-service.yaml --quiet

  rm ./backend-service.yaml
done

echo -e "Creating ${yellow}Server TLS policy${nc}."
cat << EOF > server-mtls-policy.yaml
name: "${CLUSTER}-server-mtls-policy"
serverCertificate:
  certificateProviderInstance:
    pluginInstance: google_cloud_private_spiffe
mtlsPolicy:
  clientValidationCa:
    - certificateProviderInstance:
        pluginInstance: google_cloud_private_spiffe
EOF
gcloud alpha network-security server-tls-policies import ${CLUSTER}-server-mtls-policy \
  --source=./server-mtls-policy.yaml --location=global
rm ./server-mtls-policy.yaml

echo -e "Creating ${yellow}endpoint config selector${nc}."
cat << EOF > ecs.yaml
endpointMatcher:
  metadataLabelMatcher:
    metadataLabelMatchCriteria: MATCH_ALL
    metadataLabels:
      - labelName: app
        labelValue: hipster
name: "ecs"
serverTlsPolicy: projects/${PROJECT_ID}/locations/global/serverTlsPolicies/${CLUSTER}-server-mtls-policy
type: SIDECAR_PROXY
EOF

gcloud alpha network-services endpoint-config-selectors import ${CLUSTER}-ecs \
  --source=ecs.yaml --location=global

echo -e "Creating ${yellow}authorization policy${nc}."
cat << EOF > authz_policy.yaml
action: ALLOW
name: ${CLUSTER}-authz-policy
rules:
- sources:
    - principals:
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/adservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/cartservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/checkoutservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/currencyservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/emailservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/frontend
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/paymentservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/productcatalogservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/recommendationservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/shippingservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/${K8S_NAMESPACE}/sa/shippingservice-enhanced
  destinations:
    - hosts:
        - adservice:8080
        - cartservice:8080
        - checkoutservice:8080
        - currencyservice:8080
        - emailservice:8080
        - paymentservice:8080
        - productcatalogservice:8080
        - recommendationservice:8080
        - shippingservice:8080
      paths:
        - /hipstershop.*
      ports:
        - 3550
        - 5050
        - 7000
        - 7070
        - 8080
        - 9555
        - 50051
EOF

gcloud alpha network-security authorization-policies import ${CLUSTER}-authz-policy \
  --source=authz_policy.yaml \
  --location=global
rm ./authz_policy.yaml

echo -e "Updating ${yellow}endpoint config selector${nc}."
cat << EOF >> ecs.yaml
authorizationPolicy: projects/${PROJECT_ID}/locations/global/authorizationPolicies/${CLUSTER}-authz-policy
EOF

gcloud alpha network-services endpoint-config-selectors import ${CLUSTER}-ecs \
  --source=ecs.yaml --location=global
rm ./ecs.yaml

echo -e "${yellow}Script Complete${nc}."
