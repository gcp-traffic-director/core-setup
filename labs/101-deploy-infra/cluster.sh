#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Sample script for creating a cluster for deploying microservices app
# to demo Cloud Run for Anthos
#
# PREREQUISITES
# =============
# 1) Ensure you have created a project with billing enabled
# https://support.google.com/googleapi/answer/6251787?hl=en
#
#Cannot import an array currently, need to redeclare it here
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source $SCRIPT_DIR/../../envvars
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Creating ${yellow}Pub/Sub Topics${nc}."
TOPICS=(adservice cartservice checkoutservice currencyservice emailservice frontend managementloggingservice managementservice paymentservice productcatalogservice recommendationservice shippingservice)
for topic in ${TOPICS[@]}
do
  gcloud pubsub topics create $topic &
done

echo -e "Creating ${yellow}GKE Clusters${nc}."
gcloud beta container clusters create "${CLUSTER}-${CLUSTER1ZONE}" \
  --release-channel "${CHANNEL}" \
  --zone "${CLUSTER1ZONE}" \
  --num-nodes "${NODES}" \
  --machine-type "${MACHINE}" \
  --scopes=cloud-platform \
  --workload-pool=${IDENTITY_NAMESPACE} \
  --enable-workload-certificates \
  --workload-metadata=GKE_METADATA \
  --subnetwork=default \
  --enable-ip-alias &
gcloud beta container clusters create "${CLUSTER}-${CLUSTER2ZONE}" \
  --release-channel "${CHANNEL}" \
  --zone "${CLUSTER2ZONE}" \
  --num-nodes "${NODES}" \
  --machine-type "${MACHINE}" \
  --scopes=cloud-platform \
  --workload-pool=${IDENTITY_NAMESPACE} \
  --enable-workload-certificates \
  --workload-metadata=GKE_METADATA \
  --subnetwork=default \
  --enable-ip-alias
wait

echo -e "Retrieving ${yellow}GKE Cluster Credentials${nc}."
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER1ZONE}" \
  --zone "${CLUSTER1ZONE}"
export CONTEXT_CLUSTER_1=$(kubectl config current-context)
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER2ZONE}" \
  --zone "${CLUSTER2ZONE}"
export CONTEXT_CLUSTER_2=$(kubectl config current-context)

echo -e "Registering with ${yellow}GKE Hub${nc}."
gcloud beta container hub memberships register ${CLUSTER}-${CLUSTER1ZONE} \
  --gke-cluster=${CLUSTER1ZONE}/${CLUSTER}-${CLUSTER1ZONE} \
  --enable-workload-identity
gcloud beta container hub memberships register ${CLUSTER}-${CLUSTER2ZONE} \
  --gke-cluster=${CLUSTER2ZONE}/${CLUSTER}-${CLUSTER2ZONE} \
  --enable-workload-identity

echo -e "Create ${yellow}GKE service accounts${nc}."
for account in ${ACCOUNTS[@]}
do
  kubectl create serviceaccount --namespace "default" $account \
    --cluster ${CONTEXT_CLUSTER_1}
  kubectl create serviceaccount --namespace "default" $account \
    --cluster ${CONTEXT_CLUSTER_2}
done
for account in ${ACCOUNTS[@]}
do
  gcloud iam service-accounts add-iam-policy-binding ${GSA_EMAIL} \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${IDENTITY_NAMESPACE}[default/$account]"
  kubectl annotate --namespace default serviceaccount $account \
    --cluster ${CONTEXT_CLUSTER_1} \
    iam.gke.io/gcp-service-account=${GSA_EMAIL}
  kubectl annotate --namespace default serviceaccount $account \
    --cluster ${CONTEXT_CLUSTER_2} \
    iam.gke.io/gcp-service-account=${GSA_EMAIL}
done

echo -e "Assigning IAM roles to the Google Cloud service account."
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${GSA_EMAIL}" \
  --role roles/trafficdirector.client

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${GSA_EMAIL}" \
  --role roles/monitoring.metricWriter

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${GSA_EMAIL}" \
  --role roles/cloudtrace.agent

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${GSA_EMAIL}" \
  --role roles/cloudprofiler.agent

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${GSA_EMAIL}" \
  --role roles/pubsub.admin

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${GSA_EMAIL}" \
  --role roles/clouddebugger.agent


echo -e "Retrieving ${yellow}Traffic Director Auto-Injector Package${nc}."
wget -O- https://storage.googleapis.com/traffic-director/td-sidecar-injector-xdsv3.tgz | tar xzv
pushd td-sidecar-injector-xdsv3

echo -e "Modifying ${yellow}specs/01-configmap.yaml${nc}."
sed -i "s/TRAFFICDIRECTOR_GCP_PROJECT_NUMBER:.*/TRAFFICDIRECTOR_GCP_PROJECT_NUMBER:\ \"${PROJECT_NUM}\"/g" specs/01-configmap.yaml
sed -i "s/TRAFFICDIRECTOR_NETWORK_NAME:.*/TRAFFICDIRECTOR_NETWORK_NAME:\ \"${NETWORKNAME}\"/g" specs/01-configmap.yaml
echo -e "Creating ${yellow}Certificates${nc}."
CN=istio-sidecar-injector.istio-control.svc
openssl req \
  -x509 \
  -newkey rsa:4096 \
  -keyout key.pem \
  -out cert.pem \
  -days 365 \
  -nodes \
  -subj "/CN=${CN}"

cp cert.pem ca-cert.pem

echo -e "Creating ${yellow}Namespaces for GKE Secrets${nc}."
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f specs/00-namespaces.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f specs/00-namespaces.yaml

echo -e "Creating ${yellow}GKE Cluster Secrets${nc}."
kubectl create --cluster ${CONTEXT_CLUSTER_1} secret generic istio-sidecar-injector -n istio-control \
  --from-file=key.pem \
  --from-file=cert.pem \
  --from-file=ca-cert.pem
kubectl create --cluster ${CONTEXT_CLUSTER_2} secret generic istio-sidecar-injector -n istio-control \
  --from-file=key.pem \
  --from-file=cert.pem \
  --from-file=ca-cert.pem

CA_BUNDLE=$(cat ca-cert.pem | base64 | tr -d '\n')
sed -i "s/caBundle:.*/caBundle:\ ${CA_BUNDLE}/g" specs/02-injector.yaml

echo -e "Deploying ${yellow}Sidecar Injectors to GKE Clusters${nc}."
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f specs/
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f specs/

popd

echo -e "Create the instance templates with automatic Envoy deployment."
IMAGE_ENTRYPOINT="until wget -O ./entrypoint.sh --no-cache https://storage.googleapis.com/td-demofiles/entrypoint.sh; do sleep 3; done; chmod +x ./entrypoint.sh; exec ./entrypoint.sh \"/shippingservice\""
gcloud compute instance-templates create ${CLUSTER}-tpl-${CLUSTER1ZONE} \
  --service-proxy enabled,tracing=ON,access-log=/var/log/envoy/access.log \
  --machine-type=${VIRTUAL_MACHINE} \
  --image-family=debian-10 --image-project=debian-cloud \
  --scopes=https://www.googleapis.com/auth/cloud-platform \
  --region=$CLUSTER1REGION -q \
  --metadata-from-file "startup-script=labs/101-deploy-infra/startup-script.sh" \
  --metadata docker-image="$IMAGE_LOCATION",docker-entrypoint="$IMAGE_ENTRYPOINT"
gcloud compute instance-templates create ${CLUSTER}-tpl-${CLUSTER2ZONE} \
  --service-proxy enabled,tracing=ON,access-log=/var/log/envoy/access.log \
  --machine-type=${VIRTUAL_MACHINE} \
  --image-family=debian-10 --image-project=debian-cloud \
  --scopes=https://www.googleapis.com/auth/cloud-platform \
  --region=$CLUSTER2REGION -q \
  --metadata-from-file "startup-script=labs/101-deploy-infra/startup-script.sh" \
  --metadata docker-image="$IMAGE_LOCATION",docker-entrypoint="$IMAGE_ENTRYPOINT"

echo -e "Deploying Kubernetes workloads to GKE clusters."
wget -O $SCRIPT_DIR/kubernetes-manifests.yaml \
  https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/release/v0.2.3/release/kubernetes-manifests.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -k $SCRIPT_DIR/
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -k $SCRIPT_DIR/
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f $SCRIPT_DIR/management-service.yaml

echo -e "Creating VM-based ShippingService instances in each zone."
gcloud compute instance-groups managed create ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --zone ${CLUSTER1ZONE} \
  --base-instance-name ${CLUSTER}-vm-${CLUSTER1ZONE} \
  --size=1 \
  --template=${CLUSTER}-tpl-${CLUSTER1ZONE}
gcloud compute instance-groups set-named-ports ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --zone ${CLUSTER1ZONE} \
  --named-ports "grpc:8080"
gcloud compute instance-groups managed create ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --zone ${CLUSTER2ZONE} \
  --base-instance-name ${CLUSTER}-vm-${CLUSTER2ZONE} \
  --size=1 \
  --template=${CLUSTER}-tpl-${CLUSTER2ZONE}
gcloud compute instance-groups set-named-ports ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --zone ${CLUSTER2ZONE} \
  --named-ports "grpc:8080"

echo -e "Creating health-checks and firewall rules."
gcloud compute health-checks create http ${CLUSTER}-external-frontend-hc \
  --use-serving-port \
  --check-interval=1 \
  --healthy-threshold=2 \
  --unhealthy-threshold=3 \
  --timeout=1 \
  --request-path "/_healthz"
gcloud compute health-checks create http ${CLUSTER}-managementservice-hc \
  --use-serving-port \
  --check-interval=1 \
  --healthy-threshold=2 \
  --unhealthy-threshold=3 \
  --timeout=1 \
  --request-path "/api/health"
gcloud compute firewall-rules create ${CLUSTER}-hc-${NETWORKNAME} \
  --network=${NETWORKNAME} \
  --action=ALLOW \
  --rules=tcp \
  --source-ranges=130.211.0.0/22,35.191.0.0/16
gcloud compute firewall-rules create ${CLUSTER}-int-${NETWORKNAME} \
  --network=${NETWORKNAME} \
  --action=ALLOW \
  --rules=tcp \
  --source-ranges=10.0.0.0/8,172.16.0.0/12,192.168.0.0/16

echo -e "Creating backend-services."
gcloud compute backend-services create ${CLUSTER}-external-frontend-bes \
  --global \
  --protocol HTTP \
  --health-checks ${CLUSTER}-external-frontend-hc
gcloud compute backend-services create ${CLUSTER}-managementservice-bes \
  --global \
  --protocol HTTP \
  --health-checks ${CLUSTER}-managementservice-hc
gcloud compute backend-services add-backend ${CLUSTER}-external-frontend-bes \
  --global \
  --network-endpoint-group frontend-ext-neg \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-external-frontend-bes \
  --global \
  --network-endpoint-group frontend-ext-neg \
  --network-endpoint-group-zone $CLUSTER2ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-managementservice-bes \
  --global \
  --network-endpoint-group "managementservice-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5

gcloud compute addresses create ${CLUSTER}-ext-vip \
  --global \
  --ip-version=IPV4
cat <<EOF | tee dns-spec.yaml
swagger: "2.0"
info:
  description: "Cloud Endpoints DNS"
  title: "Cloud Endpoints DNS"
  version: "1.0.0"
paths: {}
host: "${HOST}"
x-google-endpoints:
- name: "${HOST}"
  target: "$(gcloud compute addresses describe ${CLUSTER}-ext-vip --global --format="value(address)")"
EOF

gcloud endpoints services deploy dns-spec.yaml

gcloud compute ssl-certificates create ${CLUSTER}-ext-cert \
  --global \
  --domains=$HOST

gcloud compute url-maps create ${CLUSTER}-ext-map \
  --default-service ${CLUSTER}-external-frontend-bes

gcloud compute url-maps add-path-matcher ${CLUSTER}-ext-map \
  --default-service ${CLUSTER}-external-frontend-bes \
  --path-matcher-name control-api \
  --path-rules "/api/control=${CLUSTER}-managementservice-bes,/api/logging/*=${CLUSTER}-managementservice-bes"

gcloud compute target-https-proxies create ${CLUSTER}-ext-proxy \
  --url-map ${CLUSTER}-ext-map \
  --ssl-certificates=${CLUSTER}-ext-cert \
  --quic-override=ENABLE

gcloud compute forwarding-rules create ${CLUSTER}-ext-fr \
  --global \
  --address=${CLUSTER}-ext-vip \
  --target-https-proxy=${CLUSTER}-ext-proxy \
  --ports=443

envsubst < $SCRIPT_DIR/http-to-https-redirect.yaml \
| gcloud compute url-maps import ${CLUSTER}-http-to-https-redir --global -q


gcloud compute target-http-proxies create ${CLUSTER}-http-to-https-ext-proxy \
  --global \
  --url-map=${CLUSTER}-http-to-https-redir

gcloud compute forwarding-rules create ${CLUSTER}-http-to-https-fr \
  --global \
  --address=${CLUSTER}-ext-vip \
  --target-http-proxy=${CLUSTER}-http-to-https-ext-proxy \
  --ports=80

echo -e "Creating gRPC Health Checks."
gcloud compute health-checks create grpc ${CLUSTER}-grpc-hc \
  --global \
  --port=54000 \
  --check-interval=1 \
  --healthy-threshold=2 \
  --unhealthy-threshold=3 \
  --timeout=1

echo -e "Creating Traffic Director Backend Services."
for service in ${GKESERVICES[@]}
do
  gcloud compute backend-services create ${CLUSTER}-${service}-bes \
    --global \
    --load-balancing-scheme INTERNAL_SELF_MANAGED \
    --health-checks ${CLUSTER}-grpc-hc \
    --protocol grpc
done

for service in ${GKESERVICES[@]}
do
  gcloud compute backend-services add-backend ${CLUSTER}-${service}-bes \
    --global \
    --network-endpoint-group "${service}-neg" \
    --network-endpoint-group-zone $CLUSTER1ZONE \
    --balancing-mode RATE \
    --max-rate-per-endpoint 5
  gcloud compute backend-services add-backend ${CLUSTER}-${service}-bes \
    --global \
    --network-endpoint-group "${service}-neg" \
    --network-endpoint-group-zone $CLUSTER2ZONE \
    --balancing-mode RATE \
    --max-rate-per-endpoint 5 &
done
wait
gcloud compute url-maps create ${CLUSTER}-url-map \
  --global \
  --default-service ${CLUSTER}-cartservice-bes

for service in ${GKESERVICES[@]}
do
  gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
    --default-service ${CLUSTER}-${service}-bes \
    --path-matcher-name pathmatch-${service} \
    --new-hosts="${service}:8080"
done

gcloud compute target-grpc-proxies create ${CLUSTER}-grpc-proxy \
  --url-map ${CLUSTER}-url-map

gcloud compute forwarding-rules create ${CLUSTER}-td-fr \
  --global \
  --load-balancing-scheme INTERNAL_SELF_MANAGED \
  --address=0.0.0.0 \
  --target-grpc-proxy=${CLUSTER}-grpc-proxy \
  --ports 8080 \
  --network $NETWORKNAME

echo -e "Creating TCP Service for Redis."
gcloud compute health-checks create tcp ${CLUSTER}-redis-hc \
  --global \
  --port 6379

gcloud compute backend-services create ${CLUSTER}-redis-bes \
  --global \
  --load-balancing-scheme INTERNAL_SELF_MANAGED \
  --health-checks ${CLUSTER}-redis-hc \
  --protocol TCP

gcloud compute backend-services add-backend ${CLUSTER}-redis-bes --global \
  --network-endpoint-group redis-neg \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode CONNECTION --max-connections-per-endpoint=250

gcloud compute backend-services add-backend ${CLUSTER}-redis-bes --global \
  --network-endpoint-group redis-neg \
  --network-endpoint-group-zone $CLUSTER2ZONE \
  --balancing-mode CONNECTION --max-connections-per-endpoint=250

gcloud compute target-tcp-proxies create ${CLUSTER}-redis-proxy \
  --backend-service=${CLUSTER}-redis-bes

gcloud compute forwarding-rules create ${CLUSTER}-redis-fr \
  --load-balancing-scheme INTERNAL_SELF_MANAGED \
  --global \
  --address=0.0.0.0 \
  --target-tcp-proxy=${CLUSTER}-redis-proxy \
  --ports 6379 \
  --network $NETWORKNAME

gcloud compute health-checks create grpc ${CLUSTER}-shippingservice-grpc-hc \
  --global \
  --port=8080 \
  --check-interval=1 \
  --healthy-threshold=2 \
  --unhealthy-threshold=3 \
  --timeout=1

gcloud compute backend-services create ${CLUSTER}-shippingservice-vm-bes --global \
  --load-balancing-scheme INTERNAL_SELF_MANAGED \
  --connection-draining-timeout=30s \
  --port-name=grpc \
  --health-checks ${CLUSTER}-shippingservice-grpc-hc \
  --protocol grpc

gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-vm-bes \
  --global \
  --instance-group ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --instance-group-zone ${CLUSTER1ZONE}

gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-vm-bes \
  --global \
  --instance-group ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --instance-group-zone ${CLUSTER2ZONE}

gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
  --default-service ${CLUSTER}-shippingservice-vm-bes \
  --path-matcher-name pathmatch-shippingservice \
  --new-hosts="shippingservice:8080"

echo -e "Enabling Mesh services to call ${yellow}external services.${nc}"
gcloud compute backend-services import ${CLUSTER}-external-services --global -q <<EOF
name: ${CLUSTER}-external-services
loadBalancingScheme: INTERNAL_SELF_MANAGED
protocol: TCP
localityLbPolicy: ORIGINAL_DESTINATION
EOF

gcloud compute target-tcp-proxies create ${CLUSTER}-tcp-proxy \
  --backend-service=${CLUSTER}-external-services

gcloud compute forwarding-rules create ${CLUSTER}-td-fr-443 \
  --global \
  --load-balancing-scheme INTERNAL_SELF_MANAGED \
  --address=0.0.0.0 \
  --target-tcp-proxy=${CLUSTER}-tcp-proxy \
  --ports 443 \
  --network $NETWORKNAME

echo -e "Enabling ${yellow}Auto-injection in default namespace${nc}."
kubectl label --cluster ${CONTEXT_CLUSTER_1} namespace default istio-injection=enabled
kubectl label --cluster ${CONTEXT_CLUSTER_2} namespace default istio-injection=enabled
echo -e "Restarting ${yellow}Deployments${nc}."
kubectl rollout restart deployment --cluster ${CONTEXT_CLUSTER_1}
kubectl rollout restart deployment --cluster ${CONTEXT_CLUSTER_2}

echo -e "${yellow}Script Complete${nc}."
printf "https://$(gcloud endpoints services list --format="value(serviceName)" | grep "$CLUSTER")\n"
