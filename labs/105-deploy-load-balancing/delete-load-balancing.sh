#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-hipsterdemo}"
PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
REGION1="${REGION:-$(echo $CLUSTER1ZONE | cut -f1-2 -d '-')}"
NETWORKNAME="${NETWORKNAME:-default}"
CONTEXT_CLUSTER_1="gke_${PROJECT_ID}_${CLUSTER1ZONE}_${CLUSTER}-${CLUSTER1ZONE}"
CONTEXT_CLUSTER_2="gke_${PROJECT_ID}_${CLUSTER2ZONE}_${CLUSTER}-${CLUSTER2ZONE}"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Updating ${yellow}Forwarding Rule${nc}."
gcloud compute url-maps export ${CLUSTER}-url-map \
  --global --destination url-map.yaml

URL_MAP=$(cat ./url-map.yaml | tr '\n' '\r' | sed "s/  name: pathmatch-shippingservice.*weight: 50/  name: pathmatch-shippingservice/" | tr '\r' '\n')
echo "$URL_MAP" > ./url-map.yaml

gcloud compute url-maps import ${CLUSTER}-url-map \
  --global --source url-map.yaml --quiet

rm ./url-map.yaml

echo -e "Updating ${yellow}External Forwarding Rule${nc}."
gcloud compute url-maps export ${CLUSTER}-ext-map \
  --global --destination url-map.yaml

PATH_MATCH_RULES="  routeRules:
    - matchRules:
        - prefixMatch: /api
      priority: 0
      service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-managementservice-bes"
PATH_MATCH_RULES=$(echo "$PATH_MATCH_RULES" | sed "s/\\//\\\\\\//g" | tr '\n' '\r')

URL_MAP=$(cat ./url-map.yaml | tr '\n' '\r' | sed "s/  routeRules:.*selfLink/${PATH_MATCH_RULES}selfLink/" | tr '\r' '\n')
echo "$URL_MAP" > ./url-map.yaml

gcloud compute url-maps import ${CLUSTER}-ext-map \
  --global --source url-map.yaml --quiet

rm ./url-map.yaml

echo -e "Deleting ${yellow}Internal Forwarding Rule${nc}."
gcloud compute forwarding-rules delete ${CLUSTER}-int-fr \
  --region=$REGION1 --quiet

echo -e "Deleting ${yellow}Internal Target HTTP Proxy${nc}."
gcloud compute target-http-proxies delete ${CLUSTER}-int-proxy \
  --region=$REGION1 --quiet

echo -e "Deleting ${yellow}Internal URL Map${nc}."
gcloud compute url-maps delete ${CLUSTER}-int-map \
  --region=$REGION1 --quiet

echo -e "Deleting backends from ${yellow}gRPC Backend Services${nc}."
gcloud compute backend-services remove-backend ${CLUSTER}-shippingservice-enhanced-bes \
  --global --quiet \
  --network-endpoint-group "shippingservice-enhanced-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE
gcloud compute backend-services remove-backend ${CLUSTER}-shippingservice-enhanced-bes \
  --global --quiet \
  --network-endpoint-group "shippingservice-enhanced-neg" \
  --network-endpoint-group-zone $CLUSTER2ZONE

echo -e "Deleting backends from ${yellow}HTTP Backend Services${nc}."
gcloud compute backend-services remove-backend ${CLUSTER}-frontend-internal-bes \
  --region $REGION1 --quiet \
  --network-endpoint-group "frontend-internal-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE
gcloud compute backend-services remove-backend ${CLUSTER}-frontend-mobile-bes \
  --global --quiet \
  --network-endpoint-group "frontend-mobile-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE
gcloud compute backend-services remove-backend ${CLUSTER}-frontend-mobile-bes \
  --global --quiet \
  --network-endpoint-group "frontend-mobile-neg" \
  --network-endpoint-group-zone $CLUSTER2ZONE

echo -e "Deleting ${yellow}HTTP Backend Services${nc}."
gcloud compute backend-services delete ${CLUSTER}-frontend-internal-bes \
  --region $REGION1 --quiet
gcloud compute backend-services delete ${CLUSTER}-frontend-mobile-bes \
  --global --quiet

echo -e "Deleting ${yellow}gRPC Backend Services${nc}."
gcloud compute backend-services delete ${CLUSTER}-shippingservice-enhanced-bes \
  --global --quiet

echo -e "Deleting manifests to ${yellow}${CONTEXT_CLUSTER_1}${nc} and ${yellow}${CONTEXT_CLUSTER_2}${nc}."
kubectl delete --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/frontend-internal.yaml
kubectl delete --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/frontend-mobile.yaml
kubectl delete --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/frontend-mobile.yaml
kubectl delete --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
kubectl delete --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml

echo -e "${yellow}Script Complete${nc}."
