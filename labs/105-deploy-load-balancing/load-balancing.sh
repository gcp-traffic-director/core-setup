#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-microsvcdemo}"
PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
REGION1="${REGION:-$(echo $CLUSTER1ZONE | cut -f1-2 -d '-')}"
NETWORKNAME="${NETWORKNAME:-default}"
CONTEXT_CLUSTER_1="gke_${PROJECT_ID}_${CLUSTER1ZONE}_${CLUSTER}-${CLUSTER1ZONE}"
CONTEXT_CLUSTER_2="gke_${PROJECT_ID}_${CLUSTER2ZONE}_${CLUSTER}-${CLUSTER2ZONE}"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Applying manifests to ${yellow}${CONTEXT_CLUSTER_1}${nc} and ${yellow}${CONTEXT_CLUSTER_2}${nc}."
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/frontend-internal.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/frontend-mobile.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/frontend-mobile.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml

echo -e "Creating ${yellow}HTTP Backend Services${nc}."
gcloud compute backend-services create ${CLUSTER}-frontend-internal-bes \
  --health-checks ${CLUSTER}-ext-hc --protocol http \
  --load-balancing-scheme INTERNAL_MANAGED --region $REGION1
gcloud compute backend-services create ${CLUSTER}-frontend-mobile-bes \
  --global --health-checks ${CLUSTER}-ext-hc --protocol http \
  --load-balancing-scheme EXTERNAL

echo -e "Creating ${yellow}gRPC Backend Services${nc}."
gcloud compute backend-services create ${CLUSTER}-shippingservice-enhanced-bes \
  --global --health-checks ${CLUSTER}-grpc-hc --protocol grpc \
  --load-balancing-scheme INTERNAL_SELF_MANAGED

echo -e "Adding backends to ${yellow}HTTP Backend Services${nc}."
gcloud compute backend-services add-backend ${CLUSTER}-frontend-internal-bes \
  --region $REGION1 \
  --network-endpoint-group "frontend-internal-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-frontend-mobile-bes \
  --global \
  --network-endpoint-group "frontend-mobile-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-frontend-mobile-bes \
  --global \
  --network-endpoint-group "frontend-mobile-neg" \
  --network-endpoint-group-zone $CLUSTER2ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5

echo -e "Adding backends to ${yellow}gRPC Backend Services${nc}."
gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-enhanced-bes \
  --global \
  --network-endpoint-group "shippingservice-enhanced-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-enhanced-bes \
  --global \
  --network-endpoint-group "shippingservice-enhanced-neg" \
  --network-endpoint-group-zone $CLUSTER2ZONE \
  --balancing-mode RATE \
  --max-rate-per-endpoint 5

echo -e "Creating ${yellow}Internal URL Map${nc}."
gcloud compute url-maps create ${CLUSTER}-int-map \
  --default-service=${CLUSTER}-frontend-internal-bes \
  --region=$REGION1

echo -e "Creating ${yellow}Internal Target HTTP Proxy${nc}."
gcloud compute target-http-proxies create ${CLUSTER}-int-proxy \
  --url-map=${CLUSTER}-int-map \
  --url-map-region=$REGION1 \
  --region=$REGION1

echo -e "Creating ${yellow}Internal Subnet${nc}."
gcloud compute networks subnets create proxy-subnet \
  --purpose=INTERNAL_HTTPS_LOAD_BALANCER \
  --role=ACTIVE \
  --network=$NETWORKNAME \
  --range=10.100.0.0/24 \
  --region=$REGION1

echo -e "Creating ${yellow}Internal Forwarding Rule${nc}."
gcloud compute forwarding-rules create ${CLUSTER}-int-fr \
  --load-balancing-scheme=INTERNAL_MANAGED \
  --region=$REGION1 \
  --target-http-proxy=${CLUSTER}-int-proxy \
  --target-http-proxy-region=$REGION1 \
  --ports=80

echo -e "Updating ${yellow}External Forwarding Rule${nc}."
gcloud compute url-maps export ${CLUSTER}-ext-map \
  --global --destination url-map.yaml

PATH_MATCH_RULES="  routeRules:
    - matchRules:
        - prefixMatch: /api
      priority: 0
      service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-managementservice-bes
    - matchRules:
        - headerMatches:
            - headerName: user-agent
              prefixMatch: Mozilla/5.0 (iPhone
      priority: 1
      service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-frontend-mobile-bes
    - matchRules:
        - prefixMatch: /
      priority: 2
      service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-ext-bes"
PATH_MATCH_RULES=$(echo "$PATH_MATCH_RULES" | sed "s/\\//\\\\\\//g" | tr '\n' '\r')

URL_MAP=$(cat ./url-map.yaml | tr '\n' '\r' | sed "s/  pathRules:.*selfLink/${PATH_MATCH_RULES}selfLink/" | tr '\r' '\n')
echo "$URL_MAP" > ./url-map.yaml

gcloud compute url-maps import ${CLUSTER}-ext-map \
  --global --source url-map.yaml --quiet

rm ./url-map.yaml

echo -e "Updating ${yellow}Forwarding Rule${nc}."
gcloud compute url-maps export ${CLUSTER}-url-map \
  --global --destination url-map.yaml

PATH_MATCH_RULES="  routeRules:
  - priority: 0
    matchRules:
    - prefixMatch: ''
    routeAction:
      weightedBackendServices:
      - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-bes
        weight: 50
      - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-enhanced-bes
        weight: 50"
PATH_MATCH_RULES=$(echo "$PATH_MATCH_RULES" | sed "s/\\//\\\\\\//g" | tr '\n' '\r')

URL_MAP=$(cat ./url-map.yaml | tr '\n' '\r' | sed "s/  name: pathmatch-shippingservice/  name: pathmatch-shippingservice\r${PATH_MATCH_RULES}/" | tr '\r' '\n')
echo "$URL_MAP" > ./url-map.yaml

gcloud compute url-maps import ${CLUSTER}-url-map \
  --global --source url-map.yaml --quiet

rm ./url-map.yaml

echo -e "${yellow}Script Complete${nc}."
