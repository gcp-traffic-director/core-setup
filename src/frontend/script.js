(function() {
    // Load jQuery
    var script = document.createElement("SCRIPT");
    script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js";
    script.type = "text/javascript";
    script.onload = function() {
    	var $ = window.jQuery;
        $(function() {

            var requestId = $('footer').find('small').text().match(/[\n\r].*request-id:\s*([^\n\r]*)/)[1];
            var dataEndpoint = "/api";
            var services = [];
            var zone1 = "";
            var zone2 = "";
            var servicesInfo = [];

            new Promise((resolve) => setTimeout(resolve, 1000))
                .then(() => {
                    return new Promise((resolve, reject) => $.ajax({
                        method: "GET",
                        url: `${dataEndpoint}/logging/${requestId}`,
                    }).done(resolve).fail(reject));
                }).then((data) => {
                    servicesInfo = data.servicesInfo;
                }).then(() =>  {
                    return new Promise((resolve, reject) => $.ajax({
                        method: "GET",
                        url: `${dataEndpoint}/control`,
                    }).done(resolve).fail(reject));
                }).then((data) => {
                    setData(data);
                }).finally(() => {
                    initialize();
                });

            function setData(data) { 
                var locations = data.flatMap((service) => service.locations).map((location) => location.name);
                var sortedLocations = locations.sort((a, b) => locations.filter((v) => v === a).length - locations.filter((v) => v === b).length);
                zone1 = sortedLocations[0];
                zone2 = sortedLocations[1];
                services = data;
            }

            //assets
            var cogIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.45 20" height="20px" width="20px"><path fill="#ffffff" d="M17.16,11a6.19,6.19,0,0,0,.07-1,5.26,5.26,0,0,0-.08-1l2.12-1.64a.51.51,0,0,0,.12-.64l-2-3.46a.5.5,0,0,0-.61-.23l-2.49,1a7.46,7.46,0,0,0-1.69-1L12.23.43a.51.51,0,0,0-.5-.43h-4a.5.5,0,0,0-.49.43L6.86,3.07a7.78,7.78,0,0,0-1.68,1l-2.49-1a.51.51,0,0,0-.62.23l-2,3.46a.48.48,0,0,0,.13.64L2.32,9a6,6,0,0,0,0,2L.19,12.63a.51.51,0,0,0-.13.63l2,3.46a.52.52,0,0,0,.62.23l2.48-1a7.92,7.92,0,0,0,1.69,1l.38,2.64a.51.51,0,0,0,.5.43h4a.49.49,0,0,0,.49-.43l.37-2.64a7.25,7.25,0,0,0,1.69-1l2.49,1a.49.49,0,0,0,.61-.23l2-3.46a.48.48,0,0,0-.12-.64ZM9.73,13.75A3.75,3.75,0,1,1,13.48,10,3.76,3.76,0,0,1,9.73,13.75Z"/></svg>';
            var closeIcon = '<svg width="15px" height="15px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" style="enable-background:new 0 0 14 14;" xml:space="preserve"><g><rect x="6.3" y="-2.2" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -2.9046 7.0017)" fill="#FFFFFF" width="1.4" height="18.4"/><rect x="-2.2" y="6.3" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -2.8946 6.9976)" fill="#FFFFFF" width="18.4" height="1.4"/></g></svg>';
            var toggleOff = '<svg style="margin-right: 5px" width="30px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 13"><g><path fill="#605f64" d="M19.5,0H6.5a6.5,6.5,0,0,0,0,13h13a6.5,6.5,0,0,0,0-13ZM6.5,10.4a3.9,3.9,0,1,1,3.9-3.9A3.89,3.89,0,0,1,6.5,10.4Z"/><path fill="#fff" d="M6.5,10.4a3.9,3.9,0,1,1,3.9-3.9A3.89,3.89,0,0,1,6.5,10.4Z"/></g></svg>';
            var toggleOn = '<svg style="margin-right: 5px" width="30px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 13"><g><path fill="#4285f4" d="M6.5,13h13a6.5,6.5,0,0,0,0-13H6.5a6.5,6.5,0,0,0,0,13Zm13-10.4a3.9,3.9,0,1,1-3.9,3.9A3.89,3.89,0,0,1,19.5,2.6Z"/><path fill="#fff" d="M19.5,2.6a3.9,3.9,0,1,1-3.9,3.9A3.89,3.89,0,0,1,19.5,2.6Z"/></g></svg>';
            var tdLogo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAtCAYAAADV2ImkAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAFFmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDYgNzkuMTY0NjQ4LCAyMDIxLzAxLzEyLTE1OjUyOjI5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgMjIuMiAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIxLTAyLTE1VDE5OjU0OjQxKzEzOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMS0wMi0xNVQyMDoxODo1OSsxMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMS0wMi0xNVQyMDoxODo1OSsxMzowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxZjZmMTQyNC1kMDdlLTIyNDMtYjlhOS1mM2ZiNmIzZGU2Y2UiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MWY2ZjE0MjQtZDA3ZS0yMjQzLWI5YTktZjNmYjZiM2RlNmNlIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MWY2ZjE0MjQtZDA3ZS0yMjQzLWI5YTktZjNmYjZiM2RlNmNlIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDoxZjZmMTQyNC1kMDdlLTIyNDMtYjlhOS1mM2ZiNmIzZGU2Y2UiIHN0RXZ0OndoZW49IjIwMjEtMDItMTVUMTk6NTQ6NDErMTM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMi4yIChXaW5kb3dzKSIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6oelkHAAADvklEQVRYhc3ZW6hUZRQH8N+MR4560uokRPfACKqXiMjKUk4kFvRQvdRDVygpCsquUEEURhQWRQRFdJGgJASLSioTNIQKyoJeihDsAr6odPJkasdWD99Ie/bs4zj7mxH/sBn2+va39n9/l/X915pGRMjE8XgIT2FnrrNuaPbBx124D0v74Ks7IiLnWhgR45GwPSLmZ/rreuV0PiYiNkU71kfEyCAJ5yyJe7GgZLtMWiKDQ80vXRwRf0c1drWWyhEzwg2ciLexpdT2E97BSa3n+o+Mr21ExKrS6L45yPUbEYYyvnWazrDYlEb2QHCfidsw3LIN4XN8W/elOYSnmvIi4dl4sfTsIzII9+Pg6Ib9pft/c5xVEW7gRtyR47iA7LO/iDLhE/Ay3sKZ/XxRv1AkfDXW4c6WfQx349zDzuogaOJUvIR3cU6hbV7Lvg4r1SeetWbLaOIeaSRnVLzoH8zFTfgCz2C0B//R8lHEnFpMW2jiCSzD9lLbNO1hb7ake1fj7AoiVQhMlmzzML0OWRLhP/ECFmNtoW0Ltlb0GcMHuBR7u/gP7CvZ5uPo3qkmFDfd97gOD2ICG7AEz+KvUr8z8Hrr92AInWv4NFxQiy1TaokFEXFN4X5RRPwwhTorYmVENAv9RiNiW8Vza+pqiV4ePj0iNvaJ8EQkiTpQeblVihbf1Z7O/zGCJ6WN3BN61RK/SAfLeK8vqsCFWKHHiFFH/HyN52v0q8LteFoPqrGuWnsDP9foVxZCDdyP13BKy3YUbpDqHR2oS/h3vF+jX2AjdpXst+BTSSXOxgP4SIr5bcjRw6t1xthuUrIpaZZl2F1qO0tSiWuldX2+RHo5jis6qIvfsKNkm6F78tmQDp1bsa3U1pSIz2rdz8Kj+BALMZRDeAI/VhDu5vNA+3u4ShrR4kxN6pypi6TRXpJDeD/2VJDpJb3fjGulTfaNRHa4wscOSaR9mUN4Oo4t2Ub1ntjuldb1JbgSr2pXeJ9Iwuw57MzJmumcupz8ba9UAtiERZKKXI5XFKJKLuFBYBgfY410SLXhSCQ8gcd0Cn/kF1KGS7byfR2U6xhtyCG8CzdrV1zjOiNHX5FDeL+UpRwM+3RuxMqpPlQMag3PlPTAyTqz5OulJHYVvurV8aBqa3uk02upJNaLGJMq9b/W8lw3tzqEayQiPqtIj3ZHxOV1/Q60+BwR50XEHyXCKyIVw49IwiLi4YiYbJHdHBFzc/wdDsJzImJDi/AVuf4aEX0t306Fi6WizOO5jg4XYdr/SqiN/wATPftxxAoVKAAAAABJRU5ErkJggg==';

            //styles
            var mainColor = "#4285f4";
            var textColorDark = "#605f64";
            var textColorLight = "#ffffff";
            var outlineColor = "#DDDDDD";

            var buttonStyles = {
                "background-color": mainColor,
                "width": "40px",
                "height": "40px",
                "border-radius": "0",
                "border": "none",
                "cursor": "pointer",
                "display": "flex",
                "justify-content": "center",
                "align-items": "center",
                "position": "fixed",
                "top": "190px",
                "left": "20px",
                "box-shadow": "0px 0px 14px 0px rgba(50, 50, 50, 0.35)"
            };

            var focusedButtonStyle = {
                "background-color": "#2a6cd9",
                "box-shadow": "0px 0px 14px 0px rgba(50, 50, 50, 0.75)",
                "border": "none",
                "outline": "none"
            };

            var blurredButtonStyle = {
                "background-color": mainColor,
                "box-shadow": "none",
                "box-shadow": "0px 0px 14px 0px rgba(50, 50, 50, 0.35)"
            };

            var modalStyles = {
                "width": "100vw",
                "height": "100vh",
                "background-color": "rgba(0, 0, 0, 0.4)",
                "position": "absolute",
                "top": "0",
                "left": "0",
                "display": "flex",
                "justify-content": "center",
                "align-items": "center",
                "font-family": "'Roboto', sans-serif"
            };

            var panelStyles = {
                "display": "flex",
                "flex-direction": "column",
                "min-width": "750px"
            };

            var panelHeaderStyles = {
                "background": mainColor,
                "height": "45px",
                "color": textColorLight,
                "font-family": "sans-serif",
                "font-size": "24px",
                "display": "flex",
                "justify-content": "space-between",
                "align-items": "center",
                "padding": "0 18px",
                "white-space": "nowrap",
                "user-select": "none"
            };

            var panelContentStyles = {
                "background-color": "#ffffff",
                "display": "flex"
            };

            var closeButtonStyles = {
                "background": "transparent",
                "height": "14px",
                "width": "14px",
                "cursor": "pointer",
                "border": "none",
                "padding": "0",
                "display": "flex"
            };
            
            var sectionStyles = {
                "padding": "20px 18px",
                "flex": "1"
            };

            var tdStyles = {
                "top": "55px",
                "font-size": "14px",
                "height": "25px",
                "justify-content": "left",
                "padding-left": "24px"
            };

            var titleStyles = `color: ${mainColor}; font-size: 16px; font-weight: 200; margin: 0; margin-bottom: 10px;`;
            var subTitleStyles = `color: ${textColorDark}; font-size: 13px; font-weight: 200; margin-bottom: 8px; text-align: left; white-space: nowrap;`;
            var labelStyles = `color: ${textColorDark}; font-size: 16px; font-weight: 400; white-space: nowrap;`;
            var toggleLabelStyles = `color: ${textColorDark}; font-size: 12px; font-weight: 400; min-width: 20px; display: inline-block; line-height: 15px; margin: 0;`;
            var serviceNameStyles = `color: ${textColorDark}; font-size: 16px; font-weight: 600; white-space: nowrap;`;
            var tableRowStyles = `height: 25px; border-bottom: 1px solid ${outlineColor}`;
            var tdImgStyles = "height: 16px; margin-left: 4px";

            //functions

            function toggleHandler() {
                var isChecked = $(this).data('checked');
                var service = $(this).data('service');
                var location = $(this).data('location');
                $.ajax({
                  type: "PUT",
                  url: `${dataEndpoint}/control`,
                  contentType: "application/json",
                  data: JSON.stringify({
                        name: service,
                        location: location,
                        enabled: !isChecked,
                    })
                })
                  .done((data) => {
                      $(this).data('checked', !isChecked);
                      $(this).html(isChecked ? toggleOff : toggleOn);
                      $(this).append(`<label style="${toggleLabelStyles}">${isChecked ? "Off" : "On"}<label>`);
                  });
            }

            function removePanel() {
                $('#traffic-director-settings').remove();
            }

            function loadPanel() {

                //create panel layout
                var modal = $('<div id="traffic-director-settings"></div>').css(modalStyles);
                var panel = $('<div></div>').css(panelStyles);
                var panelHeader = $('<div><span>Traffic Director Settings</span></div>').css(panelHeaderStyles);
                var panelContent = $('<div></div>').css(panelContentStyles);
                var closeButton = $('<button></button>').css(closeButtonStyles).append(closeIcon).click(removePanel);
                panelHeader.append(closeButton);
                panel.append(panelHeader);

                //create content
                var informationSection = $(`<div><h3 style="${titleStyles}">LAST REQUEST INFORMATION</h3></div>`).css(sectionStyles);
                var informationTable = $(
                    `<table style="width: 100%">
                        <tr style="${tableRowStyles}">
                            <th style="${subTitleStyles} padding-right: 30px;">SERVICE</th>
                            <th style="${subTitleStyles} padding-right: 30px;">LOCATION</th>
                            <th style="${subTitleStyles} padding-right: 30px;">POD</th>
                            <th></th>
                        </tr>
                    </table>`);

                var availabilitySection = $(`<div><h3 style="${titleStyles}">INSTANCE AVAILABILITY</h3></div>`).css({ ...sectionStyles, "background-color": "#f0f0f0" })
                var availabilityTable = $(
                    `<table style="width: 100%">
                        <tr style="${tableRowStyles}">
                            <th></th>
                            <th style="${subTitleStyles}">${zone1}</th>
                            <th style="${subTitleStyles}">${zone2}</th>
                        </tr>
                    </table>`);

                services.forEach(service => {
                    var info = servicesInfo.some(s => s.name === service.name) ? servicesInfo.find(s => s.name === service.name) : null;
                    var serviceInformationRow = $(
                        `<tr style="${tableRowStyles}">
                            <td style="${serviceNameStyles} padding-right: 30px;">${service.name}</td>
                            <td style="${labelStyles} padding-right: 30px;">${info ? info.location : ''}</td>
                            <td style="${labelStyles} padding-right: 30px;">${info ? info.podId : ''}</td>
                            ${info && info.certificate ? `<td style="${labelStyles}"><a style="cursor: pointer" onclick="javascript:$('#${service.name}-cert').toggle('fast')">+</a></td>` : '<td></td>'}
                        </tr>`);
                    var serviceInformationSubRow = info && info.certificate 
                        ? $(
                            `<tr id="${service.name}-cert" style="${tableRowStyles}; display: none;">
                                <td colspan="4" style="${labelStyles}">
                                    ${info.certificate.split(';').map(x => `<div>${x}</div>`).join('')}
                                </td>
                            </td>`
                        )
                        : null;

                    var serviceAvailabilityRow = $(
                        `<tr style="${tableRowStyles}">
                            <td style="${labelStyles}">${service.name}</td>
                        </tr>`);

                    var location1 = service.locations.find(l => l.name === zone1);
                    var location2 = service.locations.find(l => l.name === zone2);

                    var zone1Availability = $(
                        `<div data-checked="${location1.enabled}" data-service="${service.name}" data-location="${location1.name}" style="display: flex; justify-content: flex-end; align-items: center;">${location1.enabled ? toggleOn : toggleOff}<label style="${toggleLabelStyles}">${location1.enabled ? "On" : "Off"}<label></div>`).click(toggleHandler);

                    var zone2Availability = $(
                        `<div data-checked="${location2.enabled}" data-service="${service.name}" data-location="${location2.name}" style="display: flex; justify-content: flex-end; align-items: center;">${location2.enabled ? toggleOn : toggleOff}<label style="${toggleLabelStyles}">${location2.enabled ? "On" : "Off"}<label></div>`).click(toggleHandler);

                    var zone1Cell = $(`<td style="${labelStyles}"></td>`);
                    var zone2Cell = $(`<td style="${labelStyles}"></td>`);

                    zone1Cell.append(zone1Availability);
                    zone2Cell.append(zone2Availability);

                    serviceAvailabilityRow.append(zone1Cell);
                    serviceAvailabilityRow.append(zone2Cell);

                    informationTable.append(serviceInformationRow);
                    if (serviceInformationSubRow) {
                        informationTable.append(serviceInformationSubRow);
                    }
                    availabilityTable.append(serviceAvailabilityRow);
                });

                //render
                informationSection.append(informationTable);
                availabilitySection.append(availabilityTable);

                panelContent.append(informationSection);
                panelContent.append(availabilitySection);

                panel.append(panelContent);
                modal.append(panel);

                $("body").append(modal);
            }

            function initialize() {
                var button = $('<button></button>')
                    .css(buttonStyles)
                    .focus(function() { $(this).css(focusedButtonStyle); })
                    .blur(function() { $(this).css(blurredButtonStyle); })
                    .click(loadPanel);
                
                button.append($(cogIcon));
                $('body').append(button);
            }

            var td = $(`<span class="platform-flag">with Traffic Director <img style="${tdImgStyles}" src="${tdLogo}"></img></span>`).css(tdStyles);
            $('.gcp-platform .platform-flag').append(td);
        });
    };
    document.getElementsByTagName('head')[0].appendChild(script);
})();