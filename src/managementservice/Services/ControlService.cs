using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using managementservice.Models;
using Microsoft.Extensions.Logging;

namespace managementservice.Services
{
  public class ControlService
    {
        private readonly ILogger<ControlService> _logger;
        private readonly GPubSubService _gPubSubService;
        private List<ServiceModel> _services;

        public ControlService(GPubSubService gPubSubService, ILogger<ControlService> logger)
        {
            _logger = logger;
            _gPubSubService = gPubSubService;
        }

        public async Task<List<ServiceModel>> GetServices()
        {
            await Initialize();
            return _services;
        }

        public async Task UpdateService(string name, string location, bool? enabled = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (string.IsNullOrEmpty(location))
            {
                throw new ArgumentNullException(nameof(location));
            }

            await Initialize();

            var currentService = _services.FirstOrDefault(s => string.Equals(name, s.Name, StringComparison.OrdinalIgnoreCase));
            if (currentService != null)
            {
                var currentLocation = currentService.Locations.FirstOrDefault(l => string.Equals(location, l.Name, StringComparison.OrdinalIgnoreCase));
                if (currentLocation != null)
                {
                    currentLocation.Enabled = enabled ?? currentLocation.Enabled;
                    await _gPubSubService.Update(name, location, currentLocation.Enabled);
                }
            }
        }

        public async Task Initialize()
        {
            if (_services != null)
            {
                return;
            }

            _services = new List<ServiceModel>();

#pragma warning disable CS4014 // Subscriber will keep running
            _gPubSubService.InitializeManagementServiceTopicSubscription(async (ImAliveServiceMessage message) =>
            {
                await UpdateService(message.Name, message.Location);
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            _logger.LogInformation("Started to listening to topic subscription.");

            foreach (var name in Constants.Services)
            {
                var model = new ServiceModel { Name = name };
                await _gPubSubService.Initialize(name);

                foreach (var location in Constants.Locations)
                {
                    await _gPubSubService.Update(name, location, enabled: true);
                    model.Locations.Add(new LocationModel { Name = location, Enabled = true });
                }

                _services.Add(model);
            }
        }
    }
}