# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# https://mcr.microsoft.com/v2/dotnet/sdk/tags/list
FROM mcr.microsoft.com/dotnet/sdk:5.0.102-ca-patch-buster-slim-amd64 as builder
WORKDIR /app
COPY managementservice.csproj .
RUN dotnet restore managementservice.csproj -r linux-x64
COPY . .
RUN dotnet publish managementservice.csproj -r linux-x64 --self-contained -c release -o /output --no-restore

# https://mcr.microsoft.com/v2/dotnet/runtime-deps/tags/list
FROM mcr.microsoft.com/dotnet/runtime-deps:5.0.2-buster-slim-amd64
RUN apt-get update && apt-get install -y libc-dev
WORKDIR /app
COPY --from=builder /output .
ENTRYPOINT ["/app/managementservice"]
