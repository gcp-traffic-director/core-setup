using System.Threading.Tasks;
using managementservice.Models;
using managementservice.Services;
using Microsoft.AspNetCore.Mvc;

namespace managementservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoggingController : ControllerBase
    {
        private readonly LoggingService _loggingService;

        public LoggingController(LoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        [HttpGet]
        [Route("{requestId}")]
        public async Task<ActionResult<RequestInfo>> GetRequestInformation([FromRoute] string requestId)
        {
            RequestInfo requestInfo = null;
            for (var i = 0; i < 5; i++)
            {
                requestInfo = _loggingService.GetRequestInformation(requestId);
                if (requestInfo != null)
                {
                    break;
                }

                await Task.Delay(1000);
            }

            return requestInfo != null ? Ok(requestInfo) : NotFound();
        }
    }
}

