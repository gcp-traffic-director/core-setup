using System;
using System.Collections.Generic;

namespace managementservice.Models
{
    public class ServiceModel
    {
        public string Name { get; set; }
        public List<LocationModel> Locations { get; } = new List<LocationModel>();
    }

    public class LocationModel
    {
        public string Name { get; set; }
        public bool Enabled { get; set; }
    }

    public class ServiceUpdateModel
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public bool Enabled { get; set; }
    }

    public class RequestInfo
    {
        public string RequestId { get; set; }
        public DateTime Created { get; set; }
        public List<ServiceRequestInfo> ServicesInfo { get; set; }
    }
}