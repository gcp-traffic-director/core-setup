#!/bin/bash

mkdir -p ./alpine
mkdir -p ./buster

docker run --rm --mount type=bind,source=$(pwd),target=/app --mount type=bind,source=$(pwd)/alpine,target=/go golang:1.15-alpine /bin/sh -c "cd /app && go mod download && go build -gcflags='-N -l' -o ./td-demo-client-musl-x64 ."
docker run --rm --mount type=bind,source=$(pwd),target=/app --mount type=bind,source=$(pwd)/buster,target=/go golang:1.15-buster /bin/sh -c "cd /app && go mod download && go build -gcflags='-N -l' -o ./td-demo-client-x64 ."
docker tag golang:1.15-buster registry.gitlab.com/gcp-traffic-director/core-setup/td-demo-client-x64
docker tag golang:1.15-alpine registry.gitlab.com/gcp-traffic-director/core-setup/td-demo-client-musl-x64
