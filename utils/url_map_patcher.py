#!/usr/bin/env python

import os
import yaml

# Read env variables
project_id = os.getenv('PROJECT_ID')
cluster_prefix = os.getenv('CLUSTER', default='microsvcdemo')

# Loading YAML files
url_map_file_name =  cluster_prefix + '-url-map-original.yaml'
url_map = yaml.safe_load(open(url_map_file_name, 'r'))

route_rule_patch_file_name = 'shippingservice-pathmatch-patch.yaml'
patch = yaml.safe_load(open(route_rule_patch_file_name, 'r'))

# Patching URL map
path_matchers = url_map['pathMatchers']
for rule in path_matchers:
    if rule['name'] == 'pathmatch-shippingservice':
        rule['routeRules'] = patch['routeRules']

# Writing patched URL map to output file
output_file_name = cluster_prefix + '-url-map-canary.yaml'
f = open(output_file_name, 'w')
f.write(yaml.dump(url_map))
f.close()

print('Patched URL map:', output_file_name)
