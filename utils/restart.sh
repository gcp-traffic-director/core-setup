#!/bin/sh

CLUSTER="${CLUSTER:-hipsterdemo}"
PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
CONTEXT_CLUSTER_1="gke_${PROJECT_ID}_${CLUSTER1ZONE}_${CLUSTER}-${CLUSTER1ZONE}"
CONTEXT_CLUSTER_2="gke_${PROJECT_ID}_${CLUSTER2ZONE}_${CLUSTER}-${CLUSTER2ZONE}"

kubectl rollout restart --cluster $CONTEXT_CLUSTER_1 deployment/adservice \
  deployment/cartservice \
  deployment/checkoutservice \
  deployment/currencyservice \
  deployment/emailservice \
  deployment/frontend \
  deployment/paymentservice \
  deployment/productcatalogservice \
  deployment/recommendationservice \
  deployment/shippingservice
kubectl rollout restart --cluster $CONTEXT_CLUSTER_2 deployment/adservice \
  deployment/cartservice \
  deployment/checkoutservice \
  deployment/currencyservice \
  deployment/emailservice \
  deployment/frontend \
  deployment/paymentservice \
  deployment/productcatalogservice \
  deployment/recommendationservice \
  deployment/shippingservice